package services;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HTMLWorker {
    /**
     * Скачивает html-страницу на рабочий стол пользователя
     * @param pageName - получаем адрес сайта от пользователя
     * @param fileName - получаем имя файла от пользователя
     */
    public void save(String pageName, String fileName) {
        String input = null;
        URL url;
        //определяем путь на конкретном компьютере
        String path = System.getProperty("user.home") + "\\Desktop";
        String name = path + "\\" + fileName + ".html";
        File newFile = new File(name);
        if (!newFile.exists()) {
            try {
                newFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            url = new URL(pageName);
            FileWriter fileWriter = new FileWriter(newFile);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            //убирает ошибки соединения с некоторыми сайтами
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
            InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(streamReader);
            while ((input = bufferedReader.readLine()) != null) {
                fileWriter.write(input);
            }
            bufferedReader.close();
            fileWriter.close();

        } catch (IOException e) {
            System.err.println("Неправильный url сайта");
            throw new IllegalArgumentException(e);
        }
    }
}
