package services;

import model.UniqueWord;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import repository.WordRepository;

import java.io.IOException;

public class TextWorker {
    private String html;
    private String text;
    private WordRepository wordRepository;

    public TextWorker(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }

    /**
     * Парсит с сайта html-страницу и обрабатывает ее
     * @param pageName - получаем адрес сайта от пользователя
     * @return только текст без кода html-файла
     */
    public String parseHtml(String pageName){
        try {
            html = Jsoup.connect(pageName).get().html();
            Document doc = Jsoup.parse(html);
            text = doc.body().text();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }

    /**
     * Разбивка текста на отдельные слова с последующей записью и подсчетом уникальных слов в БД
     */
    public void work(){
        //очистка текста от любых символов и цитат
        String cleartext = text.replaceAll("[\\p{S}\\p{Nl}\\p{Ps}\\p{Pe}\\p{Pi}\\p{Pf}]","");
        String[] parsedLines = cleartext.split("[\\s;\\d+.,:'!?—#%/()\"-]+");

        for (int i = 0; i < parsedLines.length; i++){
            UniqueWord uniqueWord = wordRepository.findByName(parsedLines[i]);

            if (uniqueWord == null){
                uniqueWord = new UniqueWord(parsedLines[i], 1);
                wordRepository.save(uniqueWord);
            } else {
                uniqueWord.setCurrentAmount(uniqueWord.getCurrentAmount() + 1);
                wordRepository.update(uniqueWord);
            }
        }
        wordRepository.print();
    }
}
