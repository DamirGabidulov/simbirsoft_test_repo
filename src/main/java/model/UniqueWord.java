package model;

import java.util.Objects;

public class UniqueWord {
    private Long id;
    private String wordName;
    private Integer currentAmount;

    public UniqueWord(Long id, String wordName, Integer currentAmount) {
        this.id = id;
        this.wordName = wordName;
        this.currentAmount = currentAmount;
    }

    public UniqueWord(String wordName, Integer currentAmount) {
        this.wordName = wordName;
        this.currentAmount = currentAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWordName() {
        return wordName;
    }

    public void setWordName(String wordName) {
        this.wordName = wordName;
    }

    public Integer getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(Integer currentAmount) {
        this.currentAmount = currentAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UniqueWord that = (UniqueWord) o;
        return Objects.equals(id, that.id) && Objects.equals(wordName, that.wordName) && Objects.equals(currentAmount, that.currentAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, wordName, currentAmount);
    }

    @Override
    public String toString() {
        return "model.UniqueWord{" +
                "id=" + id +
                ", wordName='" + wordName + '\'' +
                ", currentAmount=" + currentAmount +
                '}';
    }
}