package app;

import model.UniqueWord;
import repository.WordRepository;
import repository.WordRepositoryJdbcImpl;
import services.HTMLWorker;
import services.TextWorker;
import utils.CustomDataSource;

import javax.sql.DataSource;
import java.util.Scanner;

public class Main {

    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/html_worker_db";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "Terminal1488";

    public static void main(String[] args){

        DataSource dataSource = new CustomDataSource(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
        WordRepository wordRepository = new WordRepositoryJdbcImpl(dataSource);

//        String webPage = "https://www.simbirsoft.com/";

        System.out.println("Введите url сайта:");
        Scanner scanner = new Scanner(System.in);
        String webPage = scanner.nextLine();
        System.out.println("Введите имя файла:");
        String fileName = scanner.nextLine();

        HTMLWorker htmlWorker = new HTMLWorker();
        htmlWorker.save(webPage, fileName);

        TextWorker textWorker = new TextWorker(wordRepository);
        System.out.println(textWorker.parseHtml(webPage));
        textWorker.work();
    }
}
