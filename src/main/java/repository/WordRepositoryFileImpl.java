package repository;

import model.UniqueWord;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class WordRepositoryFileImpl implements WordRepository {
        private final String dbFileName;
        private final String sequenceFileName;

    public WordRepositoryFileImpl(String dbFileName, String sequenceFileName) {
        this.dbFileName = dbFileName;
        this.sequenceFileName = sequenceFileName;
    }

    @Override
    public UniqueWord findByName(String wordName) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(dbFileName));
            String currentString = reader.readLine();

            while (currentString != null) {
                String[] strings = currentString.split("#");
                if (wordName.equals(strings[1])) {
                    return new UniqueWord(Long.parseLong(strings[0]), strings[1], Integer.parseInt(strings[2]));
                }
                currentString = reader.readLine();
            }
            return null;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void save(UniqueWord uniqueWord) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true)); // append для продолжения записи в файл
            writer.write(generateId() + "#" + uniqueWord.getWordName() + "#" + uniqueWord.getCurrentAmount() + "\n");
            writer.close();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(UniqueWord uniqueWord) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(dbFileName));
            String currentString = reader.readLine();
            Map<Integer, UniqueWord> map = new HashMap<>();

            while (currentString != null) {

                String[] strings = currentString.split("#");
                if (uniqueWord.getWordName().equals(strings[1])) {
                    map.put(Integer.parseInt(strings[0]), uniqueWord);
                } else {
                    map.put(Integer.parseInt(strings[0]), new UniqueWord(strings[1], Integer.parseInt(strings[2])));
                }
                currentString = reader.readLine();
            }
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName));

            for(Map.Entry<Integer, UniqueWord> entry : map.entrySet()){
                writer.write(entry.getKey() + "#" + entry.getValue().getWordName() + "#" + entry.getValue().getCurrentAmount() + "\n");
            }
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void print() {
        try{
            BufferedReader reader = new BufferedReader(new FileReader(dbFileName));
            String currentString = reader.readLine();

            while (currentString != null){
                String strings[] = currentString.split("#");
                System.out.println(strings[1] + " - " + strings[2]);
                currentString = reader.readLine();
            }
        } catch (IOException e){
            throw new IllegalStateException(e);
        }
    }

    private Long generateId(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(sequenceFileName));
            String lastString = reader.readLine();
            Long id = Long.parseLong(lastString);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(sequenceFileName)); // без append для перезаписи данных
            writer.write(String.valueOf(id + 1));
            writer.close();

            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
