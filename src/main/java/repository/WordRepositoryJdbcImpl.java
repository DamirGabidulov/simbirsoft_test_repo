package repository;

import model.UniqueWord;

import javax.sql.DataSource;
import java.sql.*;

import static utils.JdbcUtil.closeJdbcObjects;

public class WordRepositoryJdbcImpl implements WordRepository {

    private DataSource dataSource;

    public WordRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    //language=SQL
    private static final String SQL_FIND_BY_NAME = "select * from unique_word where word_name = ?";
    //language=SQL
    private static final String SQL_INSERT_WORD = "insert into unique_word(word_name, current_amount) values (?, ?)";
    //language=SQL
    private static final String SQL_UPDATE_WORD = "update unique_word set word_name = ?, current_amount = ? where id = ?";
    //language=SQL
    private static final String SQL_FIND_ALL = "select * from unique_word";

    static private final RowMapper<UniqueWord> wordRowMapper = row -> new UniqueWord(
            row.getLong("id"),
            row.getString("word_name"),
            row.getInt("current_amount")
    );

    @Override
    public UniqueWord findByName(String wordName) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;

        UniqueWord uniqueWord = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_NAME);
            statement.setString(1, wordName);
            rows = statement.executeQuery();

            if (rows.next()) {
                uniqueWord = wordRowMapper.mapRow(rows);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
        return uniqueWord;
    }



    @Override
    public void save(UniqueWord uniqueWord) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_WORD,Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, uniqueWord.getWordName());
            statement.setInt(2, uniqueWord.getCurrentAmount());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1){
                throw new SQLException("Can't insert data");
            }
            generatedId = statement.getGeneratedKeys();
            if (generatedId.next()){
                uniqueWord.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }

    @Override
    public void update(UniqueWord uniqueWord) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_WORD);

            statement.setString(1, uniqueWord.getWordName());
            statement.setInt(2, uniqueWord.getCurrentAmount());
            statement.setLong(3, uniqueWord.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1){
                throw new SQLException("Can't update data");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }

    @Override
    public void print() {
        Connection connection = null;
        Statement statement = null;
        ResultSet rows = null;
        UniqueWord uniqueWord = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            rows = statement.executeQuery(SQL_FIND_ALL);

            while (rows.next()) {
                uniqueWord = wordRowMapper.mapRow(rows);
                System.out.println(uniqueWord.getWordName() + " - " + uniqueWord.getCurrentAmount());
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }

    }


}
