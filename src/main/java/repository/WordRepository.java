package repository;

import model.UniqueWord;

public interface WordRepository {
    /**
     * Поиск данного слова в БД
     * @param wordName - текущее слово из текста
     * @return уникальное слово в виде объекта UniqueWord
     */
    UniqueWord findByName(String wordName);

    /**
     * Сохраняет слово в базу данных
     * @param uniqueWord - уникальное слово
     */
    void save(UniqueWord uniqueWord);

    /**
     * Обновляет количество попаданий данного слова в тексте
     * @param uniqueWord - уникальное слово
     */
    void update(UniqueWord uniqueWord);

    /**
     * Печать в консоль всех уникальных слов и их количества в тексте
     */
    void print();
}
